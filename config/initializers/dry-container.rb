Rails.application.configure do

  if Rails.env.production?
    config.container.register(:image_loader, -> { ImageLoader })
  elsif Rails.env.development?
    config.container.register(:image_loader, -> { ImageLoader })
  elsif Rails.env.test?
    config.container.register(:image_loader, -> { FakeImageLoader })
  end
end