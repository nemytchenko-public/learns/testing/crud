class AddImageUrlToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :image_url, :string
    add_column :posts, :image_loading_state, :string
  end
end
