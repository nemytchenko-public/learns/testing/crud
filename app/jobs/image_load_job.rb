class ImageLoadJob < ApplicationJob
  queue_as :default

  def perform(post_id)
    post = Post.find_by!(id: post_id)
    img_loader = Rails.application.config.container.resolve(:image_loader)
    img_loader.load(post, post.image_url)
  end
end
