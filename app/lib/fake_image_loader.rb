module FakeImageLoader
  class << self
    def load(post, url)
      img = open('public/apple-touch-icon.png')

      post.image.attach(io: img, filename: 'stub.png')
      post.save
    end
  end
end