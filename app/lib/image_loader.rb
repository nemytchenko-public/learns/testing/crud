require 'open-uri'
require 'uri'

module ImageLoader
  class << self
    def load(post, url)
      filename = get_filename(url)
      img = open(url)

      post.image.attach(io: img, filename: filename)
      post.save
    end

    def get_filename(url)
      uri = URI.parse(url)
      File.basename(uri.path)
    end
  end
end